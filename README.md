# Lightning.py
[![Discord](https://img.shields.io/discord/527887739178188830.svg)](https://discord.gg/cDPGuYd)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline/LightSage/Lightning?label=Pipeline&logo=GitLab)](https://gitlab.com/LightSage/Lightning/pipelines/latest)

This version is the successor to Lightning.js

---

## Features

Go to https://lightning-bot.gitlab.io for a list of current features.

---
## Planned Features

<details>
<summary>Completed Features</summary>
<p>

- [x] Message Logging 
- [x] Logging: Invite Watching (soon to be rewritten)
- [x] Member Count
- [x] Announcement Maker
- [x] Moderation: Mute
- [x] Moderation: Restrictions
- [x] Moderation: Ban ID/Hackban
- [x] Moderation: Silent Kick
- [x] Moderation: Reapply Restrictions on Member Join
- [x] Admin: Fetch the guild's userlog.json file (Obsolete since v2)
- [x] Status Switching
- [x] Support Server: Special commands that help moderate the support server
- [x] Bot Owner: Blacklist Guilds and Users
- [x] Bot Owner: Search blacklist with an ID
- [x] Lightning's Timer System: Reminders/Timers
- [x] Moderation: Time Ban (relies on Lightning's Timer System)
- [x] Lightning's Timer System: Allow reminder author to delete their own reminders
- [x] Lightning's Timer System: Management Cog (PCA/PCM)
- [x] Lightning's Timer System: 6 Hour Data Backup (Now Unused since v2)
- [x] Moderation: Time Mute (relies on Lightning's Timer System)
- [x] Better Help Command

</p>
</details>

---
[![Discord Bots](https://discordbots.org/api/widget/status/532220480577470464.svg)](https://discordbots.org/bot/532220480577470464)
[![Discord Bots](https://discordbots.org/api/widget/owner/532220480577470464.svg)](https://discordbots.org/bot/532220480577470464)
## Invite

If you want to invite Lightning to your guild, use this link https://discordapp.com/api/oauth2/authorize?client_id=532220480577470464&permissions=470150390&scope=bot.

---
## License
AGPL v3 with additional terms 7b and 7c in effect.
```
# Lightning.py - The Successor to Lightning.js
# Copyright (C) 2019 - LightSage
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation at version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# In addition, clauses 7b and 7c are in effect for this program.
#
# b) Requiring preservation of specified reasonable legal notices or
# author attributions in that material or in the Appropriate Legal
# Notices displayed by works containing it; or
#
# c) Prohibiting misrepresentation of the origin of that material, or
# requiring that modified versions of such material be marked in
# reasonable ways as different from the original version
```
## Credits 

This bot uses parts and ideas from other Discord Bots:

- [Kirigiri](https://git.catgirlsin.space/noirscape/kirigiri) by Noirscape. (For staff role database layout)
- [Robocop-NG](https://github.com/reswitched/robocop-ng) by Ave and TomGER. (Some of the moderation Commands)
- [Kurisu](https://github.com/nh-server/Kurisu) by ihaveahax/ihaveamac and 916253. (For the compact logging format)
- [RoboDanny](https://github.com/Rapptz/RoboDanny) by Rapptz/Danny. (For Ideas, Unban Handler, Paginators, and other things)


Extended special thanks to:

- aspargas2
- OthersCallMeGhost